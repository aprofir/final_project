require(["esri/Map",
        "esri/views/MapView",
        "esri/layers/FeatureLayer",
        "esri/layers/WMSLayer",
        "esri/layers/GraphicsLayer",
        "esri/views/layers/LayerView",
        "esri/symbols/FillSymbol",
        "esri/widgets/Home",
        "esri/widgets/BasemapToggle",
        "esri/widgets/Legend",
        "esri/widgets/Search",
        "esri/widgets/Locate",
        "esri/widgets/Track",
        "esri/tasks/support/Query",
        "esri/tasks/QueryTask",
        "esri/Graphic",
        "dojo/dom",
        "dojo/on",
        "dojo/_base/array",
        "dojo/domReady!"
    ],

    function (Map,
              MapView,
              FeatureLayer,
              WMSLayer,
              GraphicsLayer,
              LayerView,
              FillSymbol,
              Home,
              BasemapToggle,
              Legend,
              Search,
              Locate,
              Track,
              Query,
              QueryTask,
              Graphic,
              dom,
              on) {

//popups
        var parkPopup = {
                "title": "Campus Parking",
                "content": [
                    {"type": "fields",
                        "fieldInfos":[
                            {
                                "fieldName": "Type_Rev2",
                                "label": "TYPE:",
                                "isEditable": false,
                                "tooltip": "",
                                "visible": true,
                                "format": null,
                                "stringFieldOption": "textbox"
                            },
                            {
                                "fieldName": "LotNumber",
                                "label": "LOT NUMBER:",
                                "isEditable": false,
                                "tooltip": "",
                                "visible": true,
                                "format": null,
                                "stringFieldOption": "textbox"
                            },
                            {
                                "fieldName": "Notes",
                                "label": "NOTES:",
                                "isEditable": false,
                                "tooltip": "",
                                "visible": true,
                                "format": null,
                                "stringFieldOption": "textbox"
                            }
                        ]}
                        ]
        };


        var attrctPopup = {
                "title": "{NAME}",
                "content": function(){
                    return "Open Mon-Fri {HOURS} - {HOURS} {DESCRIPTION}";
                }
        };

        var buildingPopup = {
            title: "Building",
            content: [{
                type: "fields",
                fieldInfos:[{
                    fieldName: "LONGNAME",
                    label: "Building Name",
                    visible: true
                }, {
                    fieldName: "FACILITYKE",
                    label: "Building Code",
                    visible: true
                }],
            }, {
                type: "media",
                mediaInfos: [{
                    type: "image",
                    value:{
                        sourceURL:"https://aprofir.gitlab.io/final_project/img/logo.jpg"
                    }
                }]
            }
            ]};

//layers
        var buildingLayer = new FeatureLayer({
            url:"https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/BuildingTest/FeatureServer",
            id: "Building Test",
            popupTemplate: buildingPopup
        });

        var parkingLayer = new FeatureLayer({
            url: "https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/Campus_Parking_(All)/FeatureServer/0",
            outfields: ["Type_Rev2","LotNumber","NOTES"],
            popupTemplate: parkPopup
        });
/*        
        var buildingLayer = new WMSLayer({
            url:"https://localhost:6443/arcgis/services/Buildings/MapServer/WMSServer",
            sublayers:[{
                name: "Buildings"
            }]
        });
*/
        var callBoxes = new FeatureLayer({
            url: "https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/EMCallBox/FeatureServer",
            id: "Emergency Call Boxes"
        });

        var attrctLayer = new FeatureLayer({
            url: "https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/University_Attractions/FeatureServer/0",
            outfields: ["NAME","HOURS","DESCRIPTION"],
            popupTemplate: attrctPopup
        });

//more URLs for layers (Parking Dropdowns)
        var permitPark = new FeatureLayer({
            url:"https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/Permit_Parking/FeatureServer",
            id: "Permit Parking"
        });

        var visitorPark = new FeatureLayer({
            url:"https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/Visitor_Parking/FeatureServer",
            id: "Visitor Parking"
        });

        var accessPark = new FeatureLayer({
            url:"https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/Accessible/FeatureServer",
            id: "Accessible Parking"
        });

        var motoPark = new FeatureLayer({
            url:"https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/Motorcycle_Parking/FeatureServer",
            id: "Motorcycle Parking"
        });

        var summerPark = new FeatureLayer({
            url:"https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/Parking_Summer/FeatureServer",
            id: "Summer Parking"
        });

        var gamePark = new FeatureLayer({
            url:"https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/GameDay2/FeatureServer",
            id: "Game Day Parking"
        });

//map, view, add layers

        var map = new Map({
            basemap: "streets-navigation-vector",
        });

        map.add(parkingLayer);
        map.add(buildingLayer);
        map.add(attrctLayer);

//add widgets: basemap toggle, home button, legend, search, locate, track

        var mapview = new MapView({
            container: "viewDiv",
            map: map,
            center:[-96.7838503, 44.318859],
            zoom: 16
        });

        var toggle = new BasemapToggle({
            view: mapview,
            nextBasemap: "hybrid",
        });
        mapview.ui.add(toggle,"bottom-right");

        var homeBtn = new Home({
            view: mapview,
        });
        mapview.ui.add(homeBtn, "top-left");

        var legend = new Legend({
            view: mapview
        });
        mapview.ui.add(legend, "top-right");

        let search = new Search({
            view: mapview
        });
        mapview.ui.add(search, "bottom-left");
        
        var locate = new Locate ({
            view: mapview,
            useHeadingEnabled: false,
            goToOverride: function(view, options){
                options.target.scale = 2500;
                return mapview.goTo(options.target);
            }
        });
        mapview.ui.add(locate, "top-left");
        
        var track = new Track ({
            view: mapview,
            trkgraph: new Graphic({
                symbol: {
                    type: "simple-marker",
                    size: "9px",
                    color: "green",
                    outline: {color: "#efefef", width: "1.2px" }
                }
            }),
            useHeadingEnabled: false,
            goToLocationEnabled: false,
            goToOverride: function(view, options) {
                options.target.scale = null;
                return mapview.goTo(options.target);
            }
        });
        mapview.ui.add(track, "top-left");

//Emergency Call Box Function

       on(dom.byId("callBoxBtn"),"click", viewCallBox);

       function viewCallBox()
           {map.add(callBoxes);}

//Building Dropdown Functions

        //on(dom.byId("academic"),"click", academicQuery);
        document.getElementById("academic").onclick = function(){buildingQuery('MAIN')};
        document.getElementById("food").onclick = function(){buildingQuery('FOOD SERVICE')};
        document.getElementById("event").onclick = function(){buildingQuery('EVENT')};
        document.getElementById("residence").onclick = function(){buildingQuery('RESIDENCE HALLS')};

        function buildingQuery(buildingType){
           var query = new Query({
               returnGeometry: true,
               outFields: ["*"]
           });

           query.where = "TYPE='" + buildingType + "'";
           var queryTask = new QueryTask({
               url: "https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/BuildingTest/FeatureServer/0"
           });

           queryTask.execute(query)
               .then(getResults)
               .otherwise(promiseRejected);
       }

       function getResults(response){
           displayResults(response);
       }

        function promiseRejected(){
            console.log("error")
        }

       function displayResults(response) {
           map.remove(buildingLayer);
           mapview.graphics = [];

           response.features.forEach(function (item) {
               var g = new Graphic({
                   geometry: item.geometry,
                   attributes: item.attributes,
                   symbol: {
                       type: "simple-fill",
                       color: {r:17, g:30, b:108, a:0.7},
                       width: 1.2,
                       opacity: 0.5
                   },
                   popupTemplate: buildingPopup
               });
               mapview.graphics.add(g);
           });
       }
//parking layer button functionality
        on(dom.byId("permit"),"click", hideParking);
        on(dom.byId("permit"),"click", viewPermit);
            function viewPermit()
            {map.add(permitPark)}
            
        on(dom.byId("visitor"),"click", hideParking);
        on(dom.byId("visitor"),"click", viewVisitor);
            function viewVisitor() 
            {map.add(visitorPark)}
            
        on(dom.byId("access"),"click", hideParking);
        on(dom.byId("access"),"click", viewAccess);
            function viewAccess()
            {map.add(accessPark)}

        on(dom.byId("moto"),"click", hideParking);
        on(dom.byId("moto"),"click", viewMoto);
            function viewMoto()
            {map.add(motoPark)}
            
        on(dom.byId("summer"),"click", hideParking);
        on(dom.byId("summer"),"click", viewSummer);
            function viewSummer() 
            {map.add(summerPark)}
            
        on(dom.byId("gameday"),"click", hideParking);
        on(dom.byId("gameday"),"click", viewGame);
            function viewGame()
            {map.add(gamePark)}

        on(dom.byId("turnoff"),"click", hideParking);
            function hideParking()
            {
            map.remove(permitPark);
            map.remove(visitorPark);
            map.remove(accessPark);
            map.remove(motoPark);
            map.remove(summerPark);
            map.remove(gamePark);
            }
            
//turn off all layers
        on(dom.byId("hideLayers"),"click", hideAll);
        function hideAll() {
            map.removeAll();
            mapview.graphics.removeAll();
            map.add(buildingLayer);
            map.add(parkingLayer);
            
        }

//Places dropdown functionally
    document.getElementById("eat").onclick = function()
        {showEat('EAT')};
    document.getElementById("shop").onclick = function()
        {showEat('SHOP')};
    document.getElementById("explore").onclick = function()
        {showEat('EXPLORE')};
    document.getElementById("places").onclick = function()
        {map.remove(attrctLayer);
         map.add(attrctLayer)}

     function showEat(attrctType) {
          
         mapview.graphics.removeAll();
         map.add(attrctLayer);
         var query = new Query({         
            outFields: ["*"],
            returnGeometry: true
         });
         
         query.where = "CATEGORY='" + attrctType + "'";
            
        var queryTask = new QueryTask       //define the query task
            ({
                url: "https://services2.arcgis.com/1sM9tpOC8N7GGkbw/arcgis/rest/services/UniversityAttractions/FeatureServer/0"
            });

            queryTask.execute(query)                //execute the query
           
                .then(function(result) {
                        
                        result.features.forEach(function(item)
                            {var g = new Graphic(
                                {geometry: item.geometry,
                                    attributes: item.attributes,
                                    symbol: {
                                        type: "simple-marker",
                                        style: "circle",
                                        color:[255,255,255,0],
                                        outline: {color: "black", width: "3.5px"},
                                        size: "16px"
                                    }       //close symbol
                                });     //close Graphic
                                mapview.graphics.add(g);
                            }                           //close var
                        ); //close subfunction
                    }       //close result.
                )      //close .then(function)

                .otherwise(function(e)
                {
                    console.log(e);
                });
      }
    
}); //close function and require